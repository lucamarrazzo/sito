<!doctype html>
<html lang="en">
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
        <title style="">Fiere e Sagre in Lombardia</title>
	
	<!-- Styles -->
	<link rel='stylesheet' href='assets/css/bootstrap.min.css'>
	<link rel='stylesheet' href='assets/css/animate.min.css'>
	<link rel='stylesheet' href="assets/css/font-awesome.min.css"/>
	<link rel='stylesheet' href="assets/css/style.css"/>
	
	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
			
	<!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
        
</head>
<body>
<!-- Begin Hero Bg -->
<div id="parallax">
</div>
<!-- End Hero Bg
	================================================== -->
<!-- Start Header
	================================================== -->
<header id="header" class="navbar navbar-inverse navbar-fixed-top" role="banner">
<div class="container">
	<div class="navbar-header">
		<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<!-- Your Logo -->
		<a href="http://www.regione.lombardia.it/wps/portal/istituzionale" target="blank" ><img class="logo" src="assets/img/logolombardia.png" alt="Regione Lombardia"  title="Regione Lombardia" target="blank"></a>
	</div>
	<!-- Start Navigation -->
	<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	<ul class="nav navbar-nav">
		<li>
		<a href="#hero">Home</a>
		</li>
		<li>
		<a href="#ricerca">Ricerca</a>
		</li>
		<li>
		<a href="#gallery">Maps</a>
		</li>
		<li>
		<a href="#info">Info</a>
		</li>
		
	</ul>
	</nav>
</div>
</header>

<!-- Intro
	================================================== -->
<section id="hero" class="section">
<div class="container">
	<div class="row">
		<div class="col-md-5">
			<div class="herotext">
				<h1 class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.1s">B<span class="lighter">envenuti</span></h1>
				<p class="lead wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
					Qui puoi trovare tutte le fiere e le sagre sul territorio Lombardo.
				</p>
				
			</div>
		</div>
		<div class="col-md-7">
		</div>
	</div>
</div>
</section>

<!-- Ricerca
	================================================== -->
<section id="ricerca" class="parallax section" style="background-image: url(http://www.domusblog.com/wordpress/wp-content/uploads/2014/11/Lombardia-Flag-map1.jpg);">
<div class="wrapsection">
	<div class="parallax-overlay" style="background-color: brown;opacity:0.95;"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="maintitle">

					<h3 class="section-title">TROVA LA FIERA PI&Ugrave VICINA A TE!</h3>
					<p class="lead">
						Utilizza il nostro sistema di ricerca per trovare facilmente la fiera o sagra da te desiderata
					</p>
				</div>
				                 
						<div class="wow fadeIn animated" data-wow-delay="0.3" data-wow-duration="1.5s" >
                                                   <p class="text-center"> 
                                                       <a href="RicercaFiereesagre.php" class="btn btn-default btn-lg wow fadeInLeft" role="button"> Vai alla ricerca </a>
						
                                                   </p>
                                                </div>
					
				
				
                                </div>
                                </div>
                                </div>
                                            </div>

                                       


</section>
<!-- Random
	================================================== -->
<section class="parallax section" style="background-image: url(http://www.cortonaweb.net/static/contents/eventi/sagra_bistecca.jpg);">
	<div class="wrapsection">
	<div class="parallax-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 sol-sm-12">
				<div class="maintitle">
                                    <p class="lead bottom0 wow bounceInUp">
					
					
						
					</p>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<!-- Gallery
	================================================== -->
<section id="gallery" class="parallax section" style="background-image: url(http://www.ecosostenibile.org/Immagini/mappamondo%20politico.jpg);">
<div class="wrapsection">
	<div class="parallax-overlay" style="background-color:brown;opacity:0.9;"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 sol-sm-12">
				<div class="maintitle">
					<h3 class="section-title">Mappa</h3>
					<p class="lead wow flipInX">
						Qui troverai una mappa con tutte le fiere e le sagre della Lombardia.
                                                
					</p>

           

                    
               
                                                                                        
              
            </div>
        </div>
                                             

				</div>
			</div>
			
			
		</div>
	</div>
</div>
</section>
<!-- Text Carousel
	================================================== -->
<section id="slider" class="parallax section" style="background-image: url(https://www.milanoevents.it/wp-content/uploads/2017/04/pi%C3%B9-libri-pi%C3%B9-liberi-stands.jpg);">
<div class="wrapsection">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div id="Carousel" class="carousel slide">
					<ol class="carousel-indicators">
						<li data-target="#Carousel" data-slide-to="0" class="active"></li>
						<li data-target="#Carousel" data-slide-to="1"></li>
						<li data-target="#Carousel" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<blockquote>
								<p class="lead">
									Direttamente dai database di Regione Lombardia.
								</p>
							</blockquote>
						</div>
						<div class="item">
							<blockquote>
								<p class="lead">
									Tutte le sagre e fiere sul territorio Lombardo.
								</p>
							</blockquote>
						</div>
						<div class="item">
							<blockquote>
								<p class="lead">
								Reso possibile grazie agli open data.	
								</p>
							</blockquote>
						</div>
					</div>
					<a class="left carousel-control" href="#Carousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					<a class="right carousel-control" href="#Carousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<!--info-->
<section id="info" class="parallax section" style="background-image: url(https://upload.wikimedia.org/wikipedia/commons/c/cc/Open_Data_stickers.jpg);">
<div class="wrapsection">
	<div class="parallax-overlay" style="background-color:brown;opacity:0.9;"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Title -->
				<div class="maintitle">
					<h3 class="section-title"><strong>Scopri <span class="lighter">di pi&#249</span></strong> su fiere e sagre.</h3>
					<p class="lead">
						Con il nostro servizio di ricerca potrai trovare facilmente e velocemente la fiera o sagra pi&#249  vicina a te.
					</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
                                    <span class="glyphicon glyphicon-search"></span>
					<h3>Cerca le sagre e le fiere piu vicine a te</h3>
					<div class="text-left">
						<p>
							All'interno del nostro sito internet troverai tutte le fiere e le sagre pi&#249 vicine a te.
						</p>
						<p>
							
                                                        I dati sono aggiornati in tempo reale non devi fare altro che inserire una data e una provincia.
						</p>
						<p>
							Prova la sezione ricerca!
						</p>
						
							                                                    
					
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="service-box wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
					<span class="glyphicon glyphicon-th-list"></span>
					<h3>4000 Fiere</h3>
					<p>
						In collaborazione con i database della lombardia pi&ugrave di 4000 fiere e sagre.
					</p>
					
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="service-box wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.2s">
					<span class="glyphicon glyphicon-user"></span>
					<h3>Organizzazione</h3>
					<p>
						Grazie alla ricerca per data e luogo potrai organizzare la tua visita.
					</p>
					
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="service-box wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.3s">
					<span class="glyphicon glyphicon-heart"></span>
					<h3>Guardando Al Futuro</h3>
					<p>
						Saranno circa 160 le manifestazioni di livello internazionale nel 2017. 
					</p>
					
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="service-box wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.4s">
					<span class="glyphicon glyphicon-calendar"></span>
					<h3>Prossimi Eventi</h3>
					<p>
					   Utilizza la nostra ricerca per trovare le prossime fiere.	
					</p>
					
				</div>
			</div>
		</div>
	</div>
</div>
</section>

<!-- Credits 
=============================================== -->
<section id="credits" class="text-center">

	Copyright &copy; <a href="#">Project X 2017</a> 
	<br/>Corno Andrea - Ghianda Andrea - Marrazzo Luca - Michilli Jacopo
</section>
<!-- Bootstrap core JavaScript
	================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.localScroll.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/validate.js"></script>
<script src="assets/js/common.js"></script>
</body>
</html>