<?php                          
 require_once "class.socrata.php"; 

$root_url = "www.dati.lombardia.it";
//$app_token = "iuD9KOXF92kqin6jg5JESfHiY";
$view_uid = "ykyv-6w9t";


// Create a new unauthenticated client
$socrata = new Socrata($root_url);
//$params = array();
//$params = array("\$limit" => "10");


$response= $socrata->get($view_uid);?>

<!doctype html>
<html lang="en">
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Fiera Lombardia </title>
	
	<!-- Styles -->
	<link rel='stylesheet' href='assets/css/bootstrap.min.css'>
	<link rel='stylesheet' href='assets/css/animate.min.css'>
	<link rel='stylesheet' href="assets/css/font-awesome.min.css"/>
	<link rel='stylesheet' href="assets/css/style.css"/>
	
	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
			
	<!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
</head>
<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('https://www.w3schools.com/css/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
  color:black;
}


#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 18px;

}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #bc2121;
}
</style>
<body>


<!-- End Hero Bg
	================================================== -->
<!-- Start Header
	================================================== -->
<header id="header" class="navbar navbar-inverse navbar-fixed-top" role="banner">
<div class="container">
	<div class="navbar-header">
		<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<!-- Your Logo -->

		
                <a href="http://www.regione.lombardia.it/wps/portal/istituzionale" target="blank" ><img class="logo" src="assets/img/logolombardia.png" alt="Regione Lombardia"  title="Regione Lombardia" target="blank"></a>

	</div>
	<!-- Start Navigation -->
	<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	<ul class="nav navbar-nav">
		<li>
                    <a href="index.php">Home</a>
		</li>
		
	</ul>
	</nav>
</div>
</header>


<!-- Ricerca
	================================================== -->
<section id="ricerca" class="parallax section" style="background-color: white;">
<div class="wrapsection">
	<div class="parallax-overlay" style="background-color: brown;opacity:0.95;"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="maintitle">

					<h3 class="section-title">ECCO L'ELENCO DI TUTTE LE FIERE E LE SAGRE LOMBARDE</h3>
					<p class="lead">
						Utilizza il nostro sistema di ricerca per trovare facilmente la fiera e la sagra che desideri.
					</p>
                                        
                                        
                                        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Inserisci il comune...">
                                       
                                      

                                        <table id="myTable">
  <tr class="header ">
    <th style="width:15%;">Nome</th>
    <th style="width:15%;">Comune</th>
     <th style="width:15%;">Provincia</th>
    <th style="width:20%;">Inizio Data</th>
    <th style="width:20%;">Fine Data</th>
    <th style="width:18%;">Orario Inizio</th>
    <th style="width:18%;">Orario Fine</th>
   
    
  </tr>
   <?php
        foreach ($response as $single) {
   
   $datai=($single->data_in);
   $datafin=($single->data_fine);
   $limit=explode('T',$datai);
   $limit1=explode('T',$datafin);
   
    ?>

                         <tr> 
                              
                            <td> <?php echo $nome= ($single->denom);?> </td>  
                            <td> <?php echo $comune= ($single->comune); ?> </td>
                            <td> <?php echo $provincia= ($single->prov); ?> </td>
                            <td> <?php echo $limit[0]; ?> </td>
                            <td> <?php echo $limit1[0]?> </td>
                            <td> <?php echo $orain=($single->ora_in);?> </td>
                            <td> <?php echo $orafin=($single->ora_fine);?> </td> 
                             
                        </tr>
<?php }?>
                                        </table>		
                           
                        
                      
                    
                                        </div>                 
                                    </div>
                                </div>
                        </div>
        
                </div>
        
</section>

<!-- Credits 
=============================================== -->
<section id="credits" class="text-center">

	Copyright &copy; Project X 2017</a> 
	<br/>Corno Andrea - Ghianda Andrea - Marrazzo Luca - Michilli Jacopo
</section>
<!-- Bootstrap core JavaScript
	================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.localScroll.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/validate.js"></script>
<script src="assets/js/common.js"></script>
<script>
function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i,td1;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
  
}
</script>


</body>
</html>
